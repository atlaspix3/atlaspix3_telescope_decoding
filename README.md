# ATLASPix3 Telescope Decoding

## Compilation

The script is embedded in a qt project for easier debugging. Nothing but qmake is used.

```
> qmake -o Makefile atlaspix3_decoding.pro
> make
```

Instead of using qmake, the project can also be compiled using a direct call of g++.

## Usage

After compiling, one can execute `atlaspix3_decoder example.conf` in the build directory.
The script takes one parameter which is the path of a configuration file.
The previously used command line parameters are not supported any more as they are less easy to reproduce and the function call got too complicated.

### Configuration file

The file contains the information about how to decode the data and where the data is located, as well as the output path and optional time shifts for each layer.
With the header `## Config` you precede the general parameters like `input` (file path) and `output` (file path)
And with the header `## Offset` you start the part where the timestamp offsets for the layers are specified

More details can be found in [atlaspix3_decoder.cpp#L74-L107](atlaspix3_decoder.cpp#L74-L107)

An example simple configuration file decode.conf (not all options shown):

```
input  data/202204050938_udp_beamonlancs_3GeV_5V_1.dat
output data/202204050938_udp_beamonlancs_3GeV_5V_1.txt
romode datamux
udpbug false
nsensors 4

```
